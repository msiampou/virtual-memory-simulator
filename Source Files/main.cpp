#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <ctime>
#include <iomanip>
#include <sstream>

#include "functions.h"
#include "MMU.h"

using namespace std;

int main(int argc, char* argv[]){
    string address, bit, algorithm;
    int q, max, frames, window, temp=0;

    struct timespec now = {0,0};

    if(argc == 5){
        window = 0;
        algorithm = argv[1];
        frames = atoi(argv[2]);
        q = atoi(argv[3]);
        max = atoi(argv[4]);
    }
    else if(argc == 6){
        algorithm = argv[1];
        frames = atoi(argv[2]);
        q = atoi(argv[3]);
        max = atoi(argv[4]);
        window = atoi(argv[5]);
    }
    else{
        cout << "Argc/Argv Error" << endl;
        return -1;
    }

    MMU Memory(frames,window,algorithm);

    rmoffset();
    rmof();

    ifstream Fin("visual_addresses1.txt");                    //Opening file of visual_addresses
    if(!Fin){
        cout << "Cannot open file\n" << endl;;
        return 1;
    }

    ifstream Fil("visual_addresses2.txt");                    //Opening file of visual_addresses
    if(!Fil){
        cout << "Cannot open file\n" << endl;;
        return 1;
    }

  do{
        for(int i=0;i<q;i++){
            Fin >> address >> bit;
            if(Fin.eof())
                break;
            Memory.Insert(address,1,now,bit);
            temp++;
            if(temp > max)
                break;
        }
        for(int i=0;i<q;i++){
            Fil >> address >> bit;
            if(Fil.eof())
                break;
            if(temp > max)
                break;
            Memory.Insert(address,2,now,bit);
            temp++;
        }
    }
    while(temp<max);

    cout << endl;
    cout << "Total DiskWrites: " << Memory.getDiskWrites() << endl;
    cout << "Total Page Faults: " << Memory.get_pfaults() << endl;
    cout << "Examined Entries: " << max << endl;
    cout << "Number of frames: " << frames << endl;
}
