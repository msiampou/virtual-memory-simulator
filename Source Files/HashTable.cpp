#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <climits>
#include <cstddef>

#include "HashTable.h"

const int TABLE_SIZE = 5;

using namespace std;

Entry::Entry(string k, int p, double t, string bit, int f)
            : key(k), pid(p), ts(t), RWbit(bit), frame(f)
{
    this->next = NULL;
}

string Entry::getKey(){
    return key;
}

int Entry::getPID(){
    return pid;
}

double Entry::getTS(){
    return ts;
}

int Entry::getFrame(){
    return frame;
}

string Entry::get_RWbit(){
    return RWbit;
}

Entry* Entry::getNext(){
    return next;
}

void Entry::setPID(int pid){
    this->pid = pid;
}

void Entry::setTS(double t){
    this->ts = t;
}

void Entry::set_RWbit(string bit){
    this->RWbit = bit;
}

void Entry::setNext(Entry* next){
    this->next = next;
}

//____________________________________________________________________________________//

HashMap::HashMap(int window, int f): totalFrames(f) {
    HashTable = new Entry*[TABLE_SIZE];

    int i;
    for (i = 0; i < TABLE_SIZE; i++)    //initializing the HashTable
        HashTable[i] = NULL;

    ws = new string[window];            //this array will be uses by the WS algorithm

    frames = new int[totalFrames];
    for(i=0;i<totalFrames;i++)
        frames[i] = 0;


    this->DiskWrites = 0;
    this->count = 0;
}

//**********************Search Functions*************************//

int HashMap::Search(string key, int pid, struct timespec t) {   //used by LRU
    int hash = HashFunction(key);                               //finding the position of key
    clock_gettime(CLOCK_MONOTONIC, &t);                         //time initializer

    if(HashTable[hash] == NULL)                                 //Key does not exist
        return -1;
    else {
        Entry *entry = HashTable[hash];
        while(entry!=NULL){                                     //iritating over position's list
            if((entry->getKey() == key) && (entry->getPID() == pid)){
                entry->setTS((double)t.tv_sec);                //uptate key's time
                return entry->getPID();
            }
            else
                entry = entry->getNext();
        }

        if(entry == NULL)
            return -1;
    }
    return -1;
}

double HashMap::Search(string key, int pid) {       //used by WS
    int hash = HashFunction(key);

    if(HashTable[hash] == NULL)
        return -1;
    else {
        Entry *entry = HashTable[hash];
        while(entry!=NULL){
            if((entry->getKey() == key) && (entry->getPID() == pid)){
                return entry->getTS();  //if found return key's time signature
            }
            else
                entry = entry->getNext();
        }

        if(entry == NULL)
            return -1;
    }
    return -1;
}

int HashMap::SearchWS(string key, int window){  //used by WS
    int found = 0;
    int i;
    for(i=0;i<window;i++){      //searching for a specific key inside working set
        if(key == ws[i]){
            found = 1;
            break;
        }
    }
    return found;
}

int HashMap::SearchFrame(){
    int i;
    for (i=0;i<totalFrames;i++){
        if(frames[i] == 0)
            frames[i] = 1;
            return i;
    }
    return 0;
}

//********************************Insert Functions********************************//

void HashMap::Insert(string key, int pid, struct timespec t, string RWbit) {    //used by LRU

        int hash = HashFunction(key);                           //find key's position inside HashTable
        clock_gettime(CLOCK_MONOTONIC, &t);

        this->count++;                                         //helps us to check if the memory is full

        if(HashTable[hash] == NULL)                            //if this position of array is empty
            HashTable[hash] = new Entry(key,pid,(double)t.tv_sec,RWbit,SearchFrame());   //insert the first item
        else{
            Entry *entry = HashTable[hash];                    //insert key at the end of the list
            while(entry->getNext() != NULL){
                entry = entry->getNext();
            }
            entry->setNext(new Entry(key,pid,(double)t.tv_sec,RWbit,SearchFrame()));
        }
}

int HashMap::Insert(string key, int pid, struct timespec t, string RWbit, int window) {  //used by WS

        int hash = HashFunction(key);
        clock_gettime(CLOCK_MONOTONIC, &t);

        this->count++;

        if(Search(key, pid, t) == -1){          //if key is not already inside the HashTable

            if(HashTable[hash] == NULL)
                HashTable[hash] = new Entry(key,pid,(double)t.tv_nsec,RWbit,SearchFrame());
            else{
                Entry *entry = HashTable[hash];
                while(entry->getNext() != NULL){
                    entry = entry->getNext();
                }
                entry->setNext(new Entry(key,pid,(double)t.tv_nsec,RWbit,SearchFrame()));
            }
            return wsUpdate(window,pid,key);        //updating working set
        }
        int k = wsUpdate(window,pid,key);          //if it is not just update the working set
        this->count--;
        return k;
}

int HashMap::wsUpdate(int window, int pid, string key){
    int i;

    if(this->count > window){                   //the first n=window elements are inserted to the ws without checking

        double min = Search(ws[0],pid);         //get the ts of the key which is at the first position of the ws
        int pos = 0;                            //find the less used key inside ws
        for(i=1;i<window;i++){
            if((Search(ws[i],pid) != -1) && (Search(ws[i],pid) < min)){
                min = Search(ws[i],pid);
                pos = i;
            }
        }

        int pfault = 1;                     //if the key is already inside the ws
        for(i=0;i<window;i++){
            if(key == ws[i]){
                pfault = 0;                 // no page fault occurs
                break;
            }
        }

        ws[pos].erase();                    //erase the less used key inside ws
        int found = SearchWS(key,window);
        if(found == 0){                     //if the new key is not inside the ws
            for(i=0;i<window;i++){
                if(ws[i].empty()){          //insert it at the first empty cell
                    ws[i] = key;
                    break;
                }
            }
        }
        return pfault;                      //return whether a page fault occured or not
    }
    else{                                   //if the first n=window elements are not inserted yet

        int found = SearchWS(key,window);     //search if the key is already in the ws
        if(found == 0){                       //if it is not, insert it
            for(i=0;i<window;i++){
                if(ws[i].empty()){
                    ws[i] = key;
                    break;
                }
            }
        }
        return 0;
    }
}

int HashMap::HashFunction(string key) {
	int hashVal = 0, len = key.length();

    int i;
    for(i=0; i<len; i++)                //calculate the int value of string
		hashVal += key[i];
    hashVal = hashVal % TABLE_SIZE;     //mod with the size of HashTable

    return hashVal;
}

int HashMap::IsFull(int nFrames){    //nFrames: number of frames
    if(this->count == nFrames)
        return 1;                   //return true, memory is full
    else
        return 0;                   //return false
}

int HashMap::getDiskWrites(){
    return this->DiskWrites;
}

//******************** Remove Functions ***********************//

void HashMap::Remove(int pid){      //uded by LRU
    int i;
    double minval = ULLONG_MAX;
    string min;

    for (i = 0; i < TABLE_SIZE; i++){       //search for the less used key in every cell
        if(HashTable[i]!=NULL){
            Entry *entry = HashTable[i];
            while(entry!=NULL){             //and every node of each list
                if((entry->getTS() < minval) && (pid == entry->getPID())){
                    min = entry->getKey();
                    minval = entry->getTS();
                }
                entry = entry->getNext();
            }
        }
    }
    Delete(min,pid);

}

void HashMap::Remove(int pid, int window){      //used by WS
    int i;

    for (i = 0; i < TABLE_SIZE; i++){
        if(HashTable[i]!=NULL){
            Entry *entry = HashTable[i];
            while(entry!=NULL){
                if(pid == entry->getPID()){
                    if((SearchWS(entry->getKey(),window) == 0) && (entry->getPID() == pid)){   //if key is not in the ws delete it
                        Delete(entry->getKey(),pid);
                    }
                }
                entry = entry->getNext();
            }
        }
    }
}

void HashMap::Delete(string key, int pid){

    int hash = HashFunction(key);           //find the position of key

    if (HashTable[hash] != NULL){           //if this position of HashTable is not empty
        Entry *entry = HashTable[hash];
        Entry *prev = NULL;

        while((entry->getNext()!=NULL) && (entry->getKey() != key)){    //search for the key
            prev = entry;
            entry = entry->getNext();
        }
        if((entry->getKey() == key) && (entry->getPID() == pid)){   //if found
            if(entry->get_RWbit() == "W")
                this->DiskWrites++;                                  //Disk must be informed about the change
            int pos = entry->getFrame();
            frames[pos] = 0;
            if(prev == NULL){                                       //if the previous node points to NULL
                Entry *nextEntry = entry->getNext();
                delete entry;
                HashTable[hash] = nextEntry;
            }
            else{
                Entry *nextEntry = entry->getNext();
                delete entry;
                prev->setNext(nextEntry);
            }
        }
        this->count--;
    }
}

HashMap::~HashMap() {                   //Destructor
    int i;
    Entry *prev = NULL;
    for (i = 0; i < TABLE_SIZE; i++){
        if(HashTable[i]!=NULL){
            Entry *entry = HashTable[i];
            while(entry!=NULL){
                prev = entry;
                entry = entry->getNext();
                delete prev;
            }
        }
    }
    delete[] HashTable;

    delete[] ws;

    delete[] frames;
}
