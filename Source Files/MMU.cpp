#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <ctime>
#include <iomanip>
#include <sstream>

#include "MMU.h"

using namespace std;

MMU::MMU(int f, int w, string a)
        : frames(f), window(w), replacementAlgorithm(a)
{
    this->pfaults = 0;
    Table =  new HashMap(window,frames);
}

void MMU::Insert(string address, int pid, struct timespec t, string RWbit){

    if(replacementAlgorithm == "LRU")           //use LRU algorithm
        LRU(address,pid,t,RWbit);
    else if (replacementAlgorithm == "WS")      //use WS algorithm
        WS(address,pid,t,RWbit);
}


void MMU::LRU(string address, int pid, struct timespec t, string RWbit){
    if(Table->Search(address, pid, t) == -1){       //if the page is not inside the hash table
        this->pfaults++;                            //a page fault occurs
        if(Table->IsFull(frames) == 0){             //if the memory isn't full
            Table->Insert(address, pid, t, RWbit);  //insert this page
        }
        else{
            Table->Remove(pid);                     //remove the address with the smaller time signature
            Table->Insert(address, pid, t, RWbit);  //and then insert the new page
        }
    }
}

void MMU::WS(string address, int pid, struct timespec t, string RWbit){
    if(Table->IsFull(frames) == 0){                                 //if the memory isn't full
        if((Table->Insert(address, pid, t, RWbit, window)==1))      //insert returns 1 when a page fault occurs
            this->pfaults++;
    }
    else{
        Table->Remove(pid,window);                                  //remove the address with the smaller time signature
        if((Table->Insert(address, pid, t, RWbit, window)==1))
            this->pfaults++;
    }
}

int MMU::get_pfaults(){     //return the number of page faults
    return this->pfaults;
}

int MMU::getDiskWrites(){
    return Table->getDiskWrites();
}

MMU::~MMU(){
    delete Table;
}
