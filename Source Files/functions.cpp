#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <ctime>
#include <iomanip>
#include <sstream>

#include "functions.h"

using namespace std;

void rmoffset(){
    string hexVal;
    unsigned int i;
    string bit;
    cout << "Removing offsets from bzip file. This might take some seconds. Please Wait." << endl;
    ofstream myfile("visual_addresses1.txt");

    ifstream Fin("bzip.txt");                    //Opening fie
    if(!Fin){
        cout << "Cannot open file\n" << endl;;
        return ;
    }
    do{
        Fin >> hexVal >> bit;

        for(i=0; i < 5; i++){           //remove offset
            if (myfile.is_open())
                myfile << hexVal[i];
            else
                cout << "Couldn't open" << endl;
        }
        if (myfile.is_open())
            myfile << " " << bit << endl ;
    }
    while(!Fin.eof());

    myfile.close();
    Fin.close();
}

void rmof(){
    string hexVal;
    unsigned int i;
    string bit;
    cout << "Removing offsets from trace file. This might take some seconds. Please Wait." << endl;
    ofstream myfile("visual_addresses2.txt");

    ifstream Fin("trace.txt");                    //Opening file
    if(!Fin){
        cout << "Cannot open file\n" << endl;;
        return ;
    }
    do{
        Fin >> hexVal >> bit;

        for(i=0; i < 5; i++){           //remove offset
            if (myfile.is_open())
                myfile << hexVal[i];
            else
                cout << "Couldn't open" << endl;
        }
        if (myfile.is_open())
            myfile << " " << bit << endl ;
    }
    while(!Fin.eof());

    myfile.close();
    Fin.close();
}
