objects=main.o functions.o MMU.o HashTable.o
ex2 : $(objects)
	g++ $(objects) -o ex2
main.o : MMU.h functions.h
functions.o : functions.h
MMU.o: MMU.h
HashTable.o: HashTable.h
clean:
	rm ex2 $(objects)
