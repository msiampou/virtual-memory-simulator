#include <string>
#include <ctime>

#include "HashTable.h"

class MMU {
    private:
        int frames;                         //number of frames
        int window;                         //size of working set
        std::string replacementAlgorithm;   //LRU or WS
        int pfaults;                        //number of total page faults
        HashMap *Table;                     //Memory's Hash Table

    public:
        MMU(int,int,std::string);                                    //constructor
        void Insert(std::string, int, struct timespec,std::string);  //insert new elements to the page table
        void LRU(std::string, int, struct timespec,std::string);     // LRU page replacement algorithm
        void WS(std::string, int, struct timespec,std::string);      //WS replacement algorithm
        int get_pfaults();                                           //returns the number of total page faults
        int getDiskWrites();                                         //returns the number of total Disk Writes
        ~MMU();                                                      //Destructor
};
