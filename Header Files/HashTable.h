#include <string>
#include <ctime>

class Entry {
    private:
      std::string key;
      int pid;                  //we have 2 processes
      double ts;                //time of insertion
      std::string RWbit;        //referance bit
      int frame;                //frame number
      Entry* next;

  public:
      Entry(std::string, int, double, std::string, int);
      std::string getKey();             //accessors
      int getPID();
      double getTS();
       int getFrame();
      std::string get_RWbit();
      Entry* getNext();
      void setPID(int);                 //mutators
      void setTS(double);
      void set_RWbit(std::string);
      void setNext(Entry*);
};

class HashMap {
    private:
      Entry **HashTable;                //page hash table
      int DiskWrites;
      int count;                        //counts the number of insertions so far
      int totalFrames;                  //number of total frames
      int *frames;                      //an array of frames
      std::string *ws;                  //working set

    public:
      HashMap(int,int);                                                 //constructor
      int Search(std::string, int, struct timespec);                    //searces for a specific page (LRU)
      double Search(std::string, int);                                  //searces for a specific page (WS)
      int SearchWS(std::string, int);                                   //searces for a specific page inside working set
      int SearchFrame();                                                //searces for an available frame
      void Insert(std::string, int, struct timespec,std::string);       //inserts a new page into HashTable (LRU)
      int Insert(std::string, int, struct timespec,std::string,int);    //inserts a new page into HashTable (WS)
      int HashFunction(std::string);                                    //Hashing the page number
      int IsFull(int);                                                  //checks if memory is full
      int getDiskWrites();                                              //returns the number of DiskWrites
      int wsUpdate(int,int,std::string);                                //Uptates the working set with new values
      void Remove(int);                                                 //Removes a page from memory (LRU)
      void Remove(int,int);                                             //Removes a page from memory (WS)
      void Delete(std::string,int);                                     //Deletes a specific page given by Remove
      ~HashMap();                                                       //Destructor
};
